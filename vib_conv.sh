#!/bin/bash

if [ -f FREQ_3N-6_au ];then
	rm FREQ_3N-6_au
fi

sed -n '/frequencies/,/saved_modes/p' vibrations.tag > freq1

awk '{if (NF == 3) print $0}' < freq1 > freq2

awk '{printf "%-20.6f\n %-20.6f\n %-20.6f\n" ,$1*219474.6305 ,$2*219474.6305, $3*219474.6305}' < freq2 > freq3
awk '{printf "%-20.6f\n %-20.6f\n %-20.6f\n" ,$1 ,$2, $3}' < freq2 > freq_au

awk '{print NR, $1}' < freq3 > vib_freq_cm

awk 'NR > 6 {s += 0.5*$1}END{print "Zero Point Energy :", s," au;",s*2625.5002," kJ/mol;", s*627.509608," kcal/mol"}' < freq_au > ZPE
#awk 'NR > 6 {s += 0.5*$1}END{print  s}' < freq4 > ZPE_AU

awk 'NR < 2 {print 3*$1-6}' < geom.in.xyz >> FREQ_3N-6_au
awk 'NR > 6 {print $1}' < freq_au >> FREQ_3N-6_au

rm freq1 freq2 freq3 freq_au
