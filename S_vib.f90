program inertia
      implicit none
      real(kind=8),dimension(:),allocatable:: v, vau
      integer(kind=4):: i,j,k, n_v
      real(kind=8):: kb, h, pi, T, qr, R, Sr, Sv, S, mu, mup, Bav, V0, w, S2, Ev

      kb = 1.380649E-23
      h = 6.62607004E-34
      pi = 3.14159265
      T = 298.15
      R = 8.314462
      Bav = 1.0E-44
      V0 = 2.99792E12

      open(unit=10,file='FREQ_3N-6_au',action='read')
      open(unit=11,file='Sv_298',action='write')
      open(unit=12,file='Ev_298',action='write')

      read(10,*) n_v

      allocate(v(n_v))
      allocate(vau(n_v))
      
      do i=1, n_v
        read(10,*) vau(i)
        v(i) = vau(i)*6.57966E15
      enddo

      S = 0.0
      S2 = 0.0
      Sv = 0.0
      Sr = 0.0
      w = 0.0
      Ev = 0.0


      do i=1,n_v
        Sv = R*( h*v(i)/(kb*T*(EXP(h*v(i)/(kb*T))-1)) - LOG(1 - EXP(-h*v(i)/(kb*T))))
        S2 = S2 + Sv

        mu = h/(8*pi**2*v(i))

        mup = mu*Bav/(mu+Bav)

        Sr = R*( 1/2 + LOG(SQRT((8*pi**3*mup*kb*T)/(h**2))))

        w = 1/(1 + (V0/v(i))**4)
        

        S = S + ( w*Sv + (1 - w)*Sr)

!        print*, w, (w*Sv + (1 - w)*Sr), Sv ,Sr
        Ev = Ev + h*v(i)/kb*(1/2 + 1/(EXP(h*v(i)/(kb*T))-1))
      enddo
  100 format (A,F12.4,A4,F12.4,A8,F12.4,A10)
      write(11,100) "Vibrational entropy :", T*S*1E-3*0.0003808798," au;",T*S*1E-3," kJ/mol;",&
             & T*S*1E-3*0.23900574," kcal/mol"
      write(12,100) "Thermal vibrational correction :", Ev*R*1E-3*0.0003808798," au;",Ev*R*1E-3,&
              &" kJ/mol;", Ev*R*1E-3*0.23900574," kcal/mol"
!      print*, T*S2*1E-3, T*S2*1E-3*0.23900574
!      print*, Ev*1E-3*R, Ev*R*1E-3*0.23900574

end


