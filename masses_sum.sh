#!/bin/bash

awk 'NR > 2 {print $1," "}' < geom.in.xyz > AT
awk '!a[$0]++' AT > AT2

if grep -q "C " AT2 ; then
        grep "C "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "C " AT >> n
fi
if grep -q "H " AT2 ; then
        grep "H "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "H " AT >> n
fi
if grep -q "N " AT2 ; then
        grep "N "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "N " AT >> n
fi
if grep -q "O " AT2 ; then
        grep "O "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "O " AT >> n
fi
if grep -q "F " AT2 ; then
        grep "F "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "F " AT >> n
fi
if grep -q "Na " AT2 ; then
        grep "Na "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Na " AT >> n
fi
if grep -q "Mg " AT2 ; then
        grep "Mg "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Mg " AT >> n
fi
if grep -q "P " AT2 ; then
        grep "P "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "P " AT >> n
fi
if grep -q "S " AT2 ; then
        grep "S "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "S " AT >> n
fi
if grep -q "Cl " AT2 ; then
        grep "Cl "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Cl " AT >> n
fi
if grep -q "K " AT2 ; then
        grep "K "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "K " AT >> n
fi
if grep -q "Ca " AT2 ; then
        grep "Ca "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Ca " AT >> n
fi
if grep -q "Zn " AT2 ; then
        grep "Zn "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Zn " AT >> n
fi
if grep -q "Br " AT2 ; then
        grep "Br "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Br " AT >> n
fi
if grep -q "Au " AT2 ; then
        grep "Au "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
	grep -c "Au " AT >> n
fi

paste n m > nm

awk '{s += $1*$2}END{print s}' < nm > MASSE_TOT_AMU

rm n m nm AT AT2
