#!/bin/bash

awk 'NR > 2 {print $1," "}' < geom.in.xyz > AT
awk 'NR < 2 {print $1}' < geom.in.xyz > N
awk 'NR > 2 {print $2,$3,$4}' < geom.in.xyz > XYZ
awk '!a[$0]++' AT > AT2

while read a
do
if [ $a == "C" ] ; then
        grep "C "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "H" ] ; then
        grep "H "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "N" ] ; then
        grep "N "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "O" ] ; then
        grep "O "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "F" ] ; then
        grep "F "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Na" ] ; then
        grep "Na "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Mg" ] ; then
        grep "Mg "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "P" ] ; then
        grep "P "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "S" ] ; then
        grep "S "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Cl" ] ; then
        grep "Cl "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "K" ] ; then
        grep "K "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Ca" ] ; then
        grep "Ca "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Zn" ] ; then
        grep "Zn "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Br" ] ; then
        grep "Br "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
if [ $a == "Au" ] ; then
        grep "Au "  /home/users/cpoidevin/THERMO_DFTB/Atomic_Masses | awk '{print $2}' >> m
fi
done < AT

cat N >> XYZ_MASS
paste AT XYZ m >> XYZ_MASS

rm  m AT AT2 XYZ N
